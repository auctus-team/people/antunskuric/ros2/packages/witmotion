from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    config_imus = os.path.join(
        get_package_share_directory('witmotion'),
        'config',
        'imus.yaml'
        )

    config_rviz = os.path.join(
        get_package_share_directory('witmotion'),
        'launch',
        'rviz.rviz'
        )

    return LaunchDescription([
        Node(
            package='witmotion',
            namespace='',
            executable='witmotion_node',
            name='witmotion_node',
            parameters=[config_imus]
        ),
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', config_rviz]
        )
    ])