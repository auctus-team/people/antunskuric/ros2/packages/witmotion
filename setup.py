from setuptools import setup
import os

package_name = 'witmotion'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # (os.path.join('share', package_name, 'launch'), glob('launch/*.launch.py')),
        # (os.path.join('share', package_name, 'config'), glob('config/*.yaml'))
        ('share/'+ package_name + '/launch/', ['launch/display.launch.py']),
        ('share/'+ package_name + '/launch/', ['launch/rviz.rviz']),
        ('share/'+ package_name + '/config/', ['config/imus.yaml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='antun',
    maintainer_email='you@example.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'witmotion_node = witmotion.witmotion_node:main'
        ],
    },
)
