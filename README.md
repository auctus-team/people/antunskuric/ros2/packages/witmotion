# witmotion package

This is a ROS2 package for handling the witmotion imus.

<img src="https://github.com/askuric/pywitmotion/raw/main/datasheet/image.jpg">


## Dependencies
This package depends on two python packages:
- `pywitmotion` - https://github.com/askuric/pywitmotion
- `pybluez` - https://github.com/pybluez/pybluez

Install `pybluez`
```sh
pip install pybluez
```
You might need to install the bluetooth driver if you have not got it. 


Install `pywitmotion` 
```sh
pip install git+https://github.com/askuric/pywitmotion.git
```

## Installing

Clone the package to your workspace
```sh
cd path/to/workspace
cd src
git clone git@gitlab.inria.fr:auctus-team/people/antunskuric/ros2/packages/witmotion.git
cd ..
```

build your workspace

```sh
colcon build
source install/setup.bash # source the workspace
```

run the launch file 
```sh
source install/setup.bash # source the workspace
ros2 launch witmotion display.launch.py 
```

## How to use and congifure it

In the `config/imus.yaml` file describe the list of imus you want to connect to.

Their address and the frame name their tf should be published, as well as the world frame they should be ferenced to.

The node is flexible and can handle any number of sensors, it uses multithreading to read each sensor.
