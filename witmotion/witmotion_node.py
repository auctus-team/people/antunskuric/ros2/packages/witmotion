import rclpy
import rclpy.node

from tf2_ros import TransformBroadcaster
import geometry_msgs.msg
import bluetooth
import pywitmotion as wit

import threading

# Node instance that is shared to all the function
# ideal this node code would be a pyhton class 
# then the node variable would become self 
node = None

def send_tf_msg(br, p,q,my_frame,world_frame):
    global node
    # create tf message
    t = geometry_msgs.msg.TransformStamped()

    # for timestamp you need the node instance
    t.header.stamp = node.get_clock().now().to_msg()
    t.header.frame_id = world_frame
    t.child_frame_id = my_frame
    t.transform.translation.x = p[0]
    t.transform.translation.y = p[1]
    t.transform.translation.z = p[2]
    t.transform.rotation.x = q[0]
    t.transform.rotation.y = q[1]
    t.transform.rotation.z = q[2]
    t.transform.rotation.w = q[3]
    # send the message
    br.sendTransform(t)

def imu_to_tf(soc, world_frame, imu_frame,):
    global node
    # initial read - to make sure imu is well connected
    data = soc.recv(1024)
    while True:
        # instatiate the tf sender
        # for TransformBroadcaster you need the node instance
        broadcaster = TransformBroadcaster(node)
        # read bluetooth data
        data = soc.recv(1024)
        # find the quaternion data
        for msg in reversed(data.split(b'U')):
            q = wit.get_quaternion(msg)
            if q is not None:
                # send tf message
                send_tf_msg(broadcaster, [0.0,0.0,0.0], q, imu_frame, world_frame)
                break

def main():
    global node

    rclpy.init()
    node = rclpy.create_node('witmotion_node')
    # imu parameters
    node.declare_parameters(
        namespace='',
        parameters=[
            ('world_frame', "world"),
            ('imus_ssid', None),
            ('imus_frame', None)
        ])

    # word frame get from parameter
    world_frame = node.get_parameter("world_frame").value
    # get imus list
    imu_addrs = node.get_parameter("imus_ssid").value
    imu_frames = node.get_parameter("imus_frame").value
    if not imu_addrs or not imu_frames:
        node.get_logger().error("List of imus empty - quitting")
        return    

    # connect to all imus
    for i, (imu_addr, imu_frame) in enumerate(zip(imu_addrs,imu_frames)):
        node.get_logger().info("Connecting to device : {} - {}".format("imu"+str(i),imu_frame))
        s = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        try:
            s.connect((imu_addr, 1))
            print("Device {} connected!". format("imu"+str(i)))
        except :
            print("Device {} not available!". format("imu"+str(i)))
            continue

        t = threading.Thread(target=imu_to_tf, args=(s, world_frame, imu_frame,))
        t.start()
    
if __name__ == '__main__':
    main()